#!/bin/bash

# defaults
#bunzip_cmd="bunzip2 -q"

. ~/.luchtrc

#station_id="10385" # SXL
station_id="10384"  # Tempelhof, EDDI

base_url="http://opendata.dwd.de/weather/local_forecasts/mos/MOSMIX_L/single_stations/${station_id}/kml/"
kmz_file_name="MOSMIX_L_LATEST_${station_id}.kmz"

sleep 23

cd "${lucht_path}"
curl -s "${base_url}${kmz_file_name}" -O
mv "${lucht_path}${kmz_file_name}" "${lucht_path}station.kmz"
unzip -qo "${luft_path}station.kmz"
mv ${lucht_path}MOSMIX_L_*_${station_id}.kml "${lucht_path}station.kml"

param1=$1
Rscript ${lucht_path}beeldscherm/lucht-forecast.r ${param1}


