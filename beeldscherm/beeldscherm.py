#!/usr/bin/python
# -*- coding:utf-8 -*-
import sys
import os

# load configuration
rc_vars = {}
with open(os.path.join(os.getenv("HOME"), ".luchtrc")) as rc:
    for line in rc:
        name, var = line.partition("=")[::2]
        rc_vars[name.strip()] = var.replace('"', "").rstrip()
picdir = rc_vars["lucht_path"] + "beeldscherm"
libdir = rc_vars["lucht_path"] + "beeldscherm/lib"

print("picdir = " + picdir)
print("libdir = " + libdir)

if os.path.exists(libdir):
    sys.path.append(libdir)

#import logging
#from waveshare_epd import epd4in2bc
from waveshare_epd import epd4in2
import time
from PIL import Image,ImageDraw,ImageFont

try:
    #epd = epd4in2bc.EPD()
    epd = epd4in2.EPD()
    epd.init()
    #epd.Init_4Gray()
    #epd.Clear()
    time.sleep(1)

    #HBlackimage = Image.open(os.path.join(picdir, 'test.bmp'))
    #HRYimage = Image.open(os.path.join(picdir, 'testy.bmp'))
    #epd.display(epd.getbuffer(HBlackimage), epd.getbuffer(HRYimage))
    #time.sleep(2)

    Himage = Image.open(os.path.join(picdir, 'backdrop.bmp'))
    epd.display(epd.getbuffer(Himage))
    print("display backdrop")
    time.sleep(1)
    Himage = Image.open(os.path.join(picdir, 'beeldscherm.bmp'))
    epd.display(epd.getbuffer(Himage))
    print("display beeldscherm")

    epd.sleep()

except IOError as e:
    print(e)
    
except KeyboardInterrupt:    
    print(".. aborted, exit")
    epd4in2.epdconfig.module_exit()
    exit()
