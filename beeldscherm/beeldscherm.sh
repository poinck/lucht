#!/bin/bash

. ~/.luchtrc

${lucht_path}./beeldscherm/lucht-esp.sh
Rscript ${lucht_path}./beeldscherm/beeldscherm.r
python3 ${lucht_path}./beeldscherm/beeldscherm.py

# plot for other devices and export to web server
if [ "${web}" -eq 1 ] ; then
    #${lucht_path}lucht
    	# copy of lucht.png is handled by lucht
        # ! deprecated

    #cp ${data_path}lucht.json ${lucht_webdir}
        # ! lucht.json is deprecated

    cp ${data_path}lucht_5d.csv ${lucht_webdir}
    convert -resize 400x400 -extent 430x430 -gravity center -background white -compose Copy ${lucht_path}beeldscherm/beeldscherm.bmp ${lucht_webdir}beeldscherm.png

    # copy to a remote configured in "lucht_remote_copy_command"
    # - every third time
    if [ "${remote}" -eq 1 ] ; then
        touch ${lucht_path}beeldscherm/remote
        remote_count=0
        . ${lucht_path}beeldscherm/remote
        if [ "${remote_count}" -gt 2 ] ; then
            ${lucht_remote_copy_command}
            echo "remote_count=0" > ${lucht_path}beeldscherm/remote
        else
            remote_count=$(( remote_count+1 ))
            echo "remote_count=${remote_count}" > ${lucht_path}beeldscherm/remote
        fi
    fi
fi
