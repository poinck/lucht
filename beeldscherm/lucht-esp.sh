#!/bin/bash

. ~/.luchtrc
#echo "esp_endpoint = ${esp_endpoint}"
esp_error=0
esp_json="{\"esp\": \"\"}"

curl -s --connect-timeout 20 --max-time 20 \
 	-H "Content-Type: application/json" \
	${esp_endpoint}${esp_co2} > ${data_path}./esp_co2.json.tmp
rc=$?
if [ "${rc}" -eq 0 ] ; then
    jq "." ${data_path}./esp_co2.json.tmp > /dev/null
    rc=$?
    if [ "${rc}" -eq 0 ] ; then
        mv ${data_path}./esp_co2.json.tmp ${data_path}./esp_co2.json
    else
        esp_error=1
    fi
else
    esp_error=1
fi

curl -s --connect-timeout 20 --max-time 20 \
 	-H "Content-Type: application/json" \
	${esp_endpoint}${esp_t} > ${data_path}./esp_t.json.tmp
rc=$?
if [ "${rc}" -eq 0 ] ; then
    jq "." ${data_path}./esp_t.json.tmp > /dev/null
    rc=$?
    if [ "${rc}" -eq 0 ] ; then
        mv ${data_path}./esp_t.json.tmp ${data_path}./esp_t.json
    else
        esp_error=1
    fi
else
    esp_error=1
fi

curl -s --connect-timeout 20 --max-time 20 \
 	-H "Content-Type: application/json" \
	${esp_endpoint}${esp_hum} > ${data_path}./esp_hum.json.tmp
rc=$?
if [ "${rc}" -eq 0 ] ; then
    jq "." ${data_path}./esp_hum.json.tmp > /dev/null
    rc=$?
    if [ "${rc}" -eq 0 ] ; then
        mv ${data_path}./esp_hum.json.tmp ${data_path}./esp_hum.json
    else
        esp_error=1
    fi
else
    esp_error=1
fi

if [ "${esp_error}" -eq 1 ] ; then
    esp_json="{\"esp\": \"EE!\"}"
    echo "problem with connection to ESPHome"
fi
echo "${esp_json}" > ${data_path}./esp.json
