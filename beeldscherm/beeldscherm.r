#!/usr/bin/Rscript

require(rjson)
require(data.table)

# load configuration
rc <- read.csv(file = "~/.luchtrc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
rc_table <- data.table(rc, key = "key")
#print(str(rc_table))
lucht_path <- rc_table["lucht_path"]$value
data_path <- rc_table["data_path"]$value
station_height <- as.integer(rc_table["station_height"]$value)
beeldscherm_tz <- rc_table["beeldscherm_tz"]$value
cat("lucht_path = ", lucht_path, "\n")
cat("data_path = ", data_path, "\n")
cat("station_height =", station_height, "\n")

calculate_dewpoint <- function(tmp, hum) {
    dew <- NA

    if (!is.na(hum) && !is.na(tmp)) {
        # derived from Magnus formula using Sonntag (1990) coefficients
        c <- 243.12
        b <- 17.62
        dew <- c * ((log(hum / 100) + ((b * tmp)/(c + tmp))) / (b - log(hum / 100) - ((b * tmp)/(c + tmp))))
    }

    return(dew)
}

calculate_dews <- function(tmps, hums) {
    l <- length(tmps)
    dews <- rep(NA, l)

    for (i in c(1:l)) {
        dews[i] <- calculate_dewpoint(tmps[i], hums[i])
    }

    return(dews)
}

calculate_reduced_air_pressure <- function(tmp, hum, hpa, h) {
    # based on equation of DWD (Deutscher Wetterdienst)
    r <- NA

    g0 <- 9.80665   # acceleration caused by earth gravity (m/s²)
    Ch <- 0.12      # change of water wapor pressure (K/hPa)
    Rair <- 287.05  # gas constant of dry air (m²/s²K)
    a <- 0.0065     # vertical temperature gradient (K/m)

    Th <- tmp + 273.15
    E <- hpa * (hum / 100)
    r <- hpa * exp(g0 / (Rair * (Th + Ch * E + a * (h / 2))) * h)

    return(r)
}

reduce_air_pressure <- function(rt, tmp, hum, hpa, h) {
    l <- length(rt)
    r <- rep(NA, l)

    for (i in c(1:l)) {
        if (!is.na(tmp[i]) && !is.na(hum[i]) && !is.na(hpa[i])) {
            r[i] <- calculate_reduced_air_pressure(tmp[i], hum[i], hpa[i], h)
        }
    }

    return(r)
}

bmp(filename = paste0(lucht_path, "./beeldscherm/beeldscherm.bmp"),
    width = 400, height = 300,
    units = "px",
    pointsize = 18,
    family = "B612"
)

par(xpd = NA, mar = c(0, 0, 0, 0))

now <- as.POSIXct(Sys.time(), tz = "UTC")

# test
#now <- as.POSIXct("2024-04-20 22:24:04", tz = "UTC")

now_date_formatted <- strftime(now, "%a %Y-%m-%d W%W", tz = beeldscherm_tz)
now_time_formatted <- strftime(now, "%H:%M", tz = beeldscherm_tz)
#day_week <- strftime(now, "W%W", tz = beeldscherm_tz)

current_hour = as.integer(strftime(now, "%H", tz = beeldscherm_tz))
day_fraction = (24 - current_hour) / 24
day_ticks = seq(1, 4)
day_ticks_in_plot = day_ticks + day_fraction
#cat("current_hour:", current_hour, "\n")

xrange <- range(c(1, 5))
yrange <- range(c(1, 5))

plot(2, 3.5, lwd = 6, col = "white",
    axes = FALSE, cex = 1.5,
    panel.first = {
        abline(h = 3.5, lwd = 2, lty = 1)
        abline(v = 2, lwd = 2, lty = 1)
        abline(h = 1.5, lwd = 1, lty = 3)
    },
    ylim = yrange, xlim = yrange
)
text(x = 2.0, y = 3.65, now_date_formatted, font = 1, pos = 4)
text(x = 2, y = 4.0, now_time_formatted, font = 2, pos = 2, cex = 1.5)

# get esp data and state
tryCatch({
    co2 <- fromJSON(file = paste0(data_path, "./esp_co2.json"))
}, error = function(e) {
    co2 = data.frame(state = "NA", value = NA)
})
#print(str(co2))
tryCatch({
    t <- fromJSON(file = paste0(data_path, "./esp_t.json"))
}, error = function(e) {
    t = data.frame(state = "NA", value = NA)
})
#print(str(t))
tryCatch({
    hum <- fromJSON(file = paste0(data_path, "./esp_hum.json"))
}, error = function(e) {
    hum = data.frame(state = "NA", value = NA)
})
#print(str(hum))
tryCatch({
    esp <- fromJSON(file = paste0(data_path, "./esp.json"))
}, error = function(e) {
    esp = data.frame(esp = "NA")
})
#print(str(esp))

# save new esp measurement to file at data directory
# - only if none of the states are NA
if (esp$esp == "") {
    if (t$state == "NA") {
        esp$esp <- "ET"
        t$value <- NA
    }
    if (hum$state == "NA") {
        esp$esp <- "EH"
        hum$value <- NA
    }
    if (co2$state == "NA") {
        esp$esp <- "EC"
        co2$value <- NA
    }
    esp_current_data <- data.frame(
        time = as.POSIXct(now, tz = "UTC"),
        co2 = as.numeric(co2$value),
        t = as.numeric(t$value),
        hum = as.numeric(hum$value)
    )
} else {
    esp_current_data <- data.frame(
        time = as.POSIXct(now, tz = "UTC"),
        co2 = NA,
        t = NA,
        hum = NA
    )
}
esp_file <- paste0(data_path, "esp.csv")
if (file.exists(esp_file)) {
    cat("read", esp_file, "..\n")
    esp_data <- read.csv(esp_file)
    esp_data$time <- as.POSIXct(esp_data$time, tz = "UTC")
    esp_l <- length(esp_data$co2)
    cat("esp data length =", esp_l, "\n")

    # add current data
    esp_data <- rbind(esp_data, esp_current_data)
        # fixme
        # this will get slow someday
        # - add archiving of old data
    #print(str(esp_current_data))
    #print(str(esp_data))
} else {
    cat("write new", esp_file, "..\n")
    esp_data <- esp_current_data
}
write.csv(esp_data, file = esp_file, row.names = FALSE)

if (t$state == "NA") {
    t$value <- esp_data$t[esp_l-1]
}
if (hum$state == "NA") {
    hum$value <- esp_data$hum[esp_l-1]
}
if (co2$state == "NA") {
    co2$value <- esp_data$co2[esp_l-1]
}
text(x = 3.9, y = 4.44, round(as.numeric(t$value), digits = 1),
    font = 1, cex = 1.8, pos = 4)
text(x = 3.9, y = 4.9, " t", font = 3, cex = .9, pos = 4)
text(x = 3.9, y = 4.0, round(as.numeric(hum$value), digits = 0),
    cex = 1.5, pos = 4)
text(x = 4.32, y = 3.96, "%", cex = 1, pos = 4)
text(2, 4.44, co2$value, font = 2, cex = 1.8, pos = 2)
text(x = 2, y = 4.9, "co2", font = 3, cex = .9, pos = 2)

# test
#esp$esp = "TT!"

text(x = 0.85, y = 4.9, esp$esp, font = 2, cex = 0.9, pos = 4)

# read rsynched data
lucht_file <- paste(data_path, "lucht_5d.csv", sep = "")
lucht <- read.csv(lucht_file, skipNul = TRUE)
names(lucht) <- c(
    "time",
    "pm1",
    "pm4",
    "sps_err",
    "ta1",
    "hum1",
    "sht1_err",
    "ta2",
    "hum2",
    "sht2_err",
    "ta",
    "hum",
    "sht_err",
    "t_logger",
    "p",
    "icp_err",
    "not_used1",
    "not_used2",
    "not_used3"
)

# current values
lucht_length = length(lucht$time)
text(x = 2, y = 4.44, round(lucht$ta[lucht_length], digits = 1), font = 2, cex = 1.8, pos = 4)
text(x = 2, y = 4.9, " ta", font = 3, cex = .9, pos = 4)
text(x = 3.55, y = 4.9, " pm4", font = 3, cex = .9, pos = 2)
text(x = 2, y = 4.0, round(lucht$hum[lucht_length], digits = 0), font = 2, cex = 1.5, pos = 4)
text(x = 2.45, y = 3.96, "%", font = 2, cex = 1, pos = 4)
text(x = 2, y = 3.64, "hPa", font = 3, pos = 2, cex = .9)
text(x = 1.65, y = 3.65, round(lucht$p[lucht_length], digits = 0), font = 1, pos = 2)

# particulate matter pollution
# - "P" gets darker with increasing pm1 ceoncentration and when there is
#   currently an event with larger particles
pm <- lucht$pm4[lucht_length] - lucht$pm1[lucht_length]
pm_255 <- 255 - pm * 50 - lucht$pm1[lucht_length]
    # consider larger particles but also honor high background pollution
if (pm_255 < 0) {
    pm_255 <- 0
}
if (pm_255 > 255) {
    pm_255 <- 255
}
pm_255 <- pm_255 / 255
pm_col <- rgb(pm_255, pm_255, pm_255, 1)
#print(pm_col)
text(x = 4.85, y = 3.9, "P", font = 2, col = pm_col, cex = 3)

# save PM4 for later use
pm4 <- lucht$pm4[lucht_length]

lucht$time <- as.POSIXct(strptime(lucht$time, "%Y-%m-%dT%H:%M+00:00",
    tz = "UTC"))
daysins <- 86400 # 1 day
start_end_seq <- seq(
    from = round(as.POSIXct(lucht$time[length(lucht$ta)],
        format = "%Y-%m-%dT%H:00:00",
        tz = "UTC"), units = "hours") - daysins,
    to = lucht$time[length(lucht$ta)],
    by = "600 sec")
#print(start_end_seq)

# gather daytick in measurements
# - used for min/max display
measurement_hours = as.integer(strftime(start_end_seq, "%H"))
daytick_in_measurements = match(0, measurement_hours)
if (daytick_in_measurements == 1) {
    # next daytick, if a day just started
    daytick_in_measurements = length(measurement_hours)
}
#print(measurement_hours)
#cat("daytick_in_measurements:", daytick_in_measurements, "\n")
#cat("measurement_hours (length):", length(measurement_hours), "\n")

# check logger time (time of last measurement)
# - print error on beeldscherm "L!", if last measurment is more than
#   1h away from now
# - show diff in not regular font when less then 1h and
#   in bold when > 1h.
# - don't show if within one rsync interval
last_time <- as.POSIXct(lucht$time[length(lucht$ta)], tz = "UTC")
diff_time <- now - last_time
max_diff_time <- as.difftime(1, units = "hours")
min_diff_time <- as.difftime(11, units = "mins")
diff_error <- ""
diff_msg_font <- 1
if (diff_time > max_diff_time) {
    diff_error <- "L!"
    diff_msg_font <- 2
}

diff_msg <- round(as.numeric(diff_time, units = "mins"), digits = 0)
if (diff_msg > 99) {
    diff_msg <- "L>99"
} else {
    diff_msg <- paste0(diff_error, diff_msg)
}

# test
#diff_time <- 100000
#diff_msg <- "L!"

if (diff_time > min_diff_time) {
    text(x = 5.15, y = 4.9, diff_msg, font = diff_msg_font, cex = 0.9, pos = 2)
}
#print(diff_msg)

# ta
tmp_time_aggr <- cut(x = lucht$time, breaks = start_end_seq)
tmp_aggr <- aggregate(lucht$ta, FUN = mean,
    by = list(time_aggr = tmp_time_aggr), na.rm = TRUE)
tmp_aggr$x[tmp_aggr$x < -80] <- NA
#print(tmp_aggr$time_aggr)
mtime_tr <- seq(from = 1, to = 2, length.out = length(tmp_aggr$x))
mt_tr <- tmp_aggr$x * .05 + 1.5
    # translate into ranges of beeldscherm

# hum
hum_time_aggr <- cut(x = lucht$time, breaks = start_end_seq)
hum_aggr <- aggregate(lucht$hum, FUN = mean,
    by = list(time_aggr = hum_time_aggr), na.rm = TRUE)
hum_aggr$x <- hum_aggr$x
dew_aggr_x <- calculate_dews(tmp_aggr$x, hum_aggr$x)
mtd_tr <- dew_aggr_x * .05 + 1.5

# hpa
hpa_time_aggr <- cut(x = lucht$time, breaks = start_end_seq)
hpa_aggr <- aggregate(lucht$p, FUN = mean,
    by = list(time_aggr = hpa_time_aggr), na.rm = TRUE)
hpa_aggr$x <- hpa_aggr$x
hpa_aggr$x <- reduce_air_pressure(hpa_aggr$time_aggr, tmp_aggr$x, hum_aggr$x,
    hpa_aggr$x, station_height)
hpa_aggr$x <- hpa_aggr$x - 1013.25 # deviation from normal pressure
hpa_aggr$x[tmp_aggr$x < -80] <- NA
mhpa_tr <- hpa_aggr$x * .05 + 1.5
#print(str(mhpa_tr))

# daytick: measurements
xmt = day_ticks_in_plot[1]
polygon(
    x = c(xmt, xmt),
    y = c(.5, 3.5),
    border = "#878787", lwd = 2, lty = 1
)

# plot measurements
par(new = TRUE)
plot(mtime_tr, mhpa_tr, lty = 2,
    lwd = 2, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(mtime_tr, mtd_tr, col = "white",
    lwd = 3, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(mtime_tr, mt_tr, col = "white",
    lwd = 11, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(mtime_tr, mtd_tr,
    lwd = 1.5, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(mtime_tr, mt_tr,
    lwd = 5, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)

# limit display of pm4 to 99
if (pm4 > 99) {
    pm4 <- 99
}
text(x = 3.6, y = 4.44, round(pm4, digits = 0), font = 1,
    cex = 1.8, pos = 2)

# read forecast: ICON (preprocessed from station kml)
cat("load forecast\n")
load(paste0(lucht_path, "forecast.rda"))

daysins <- 259200 # 3 days
now_3d <- round(as.POSIXct(now, format = "%Y-%m-%dT%H:00:00",
    tz = "UTC"), units = "hours") + daysins
ff <- f[f$time >= now & f$time < now_3d, ]
ftime_tr <- seq(from = 2, to = 5, length.out = length(ff$tmp))
#cat("ftime_tr.length:", length(ftime_tr), "\n")
fta_tr <- ff$tmp * .05 + 1.5
    # translate into ranges of beeldscherm
max_fta <- max(ff$tmp, na.rm = TRUE)
move_wind_offset <- 0
if (max_fta > 23) {
    # prevent overlapping temperature graphs on wind roses
    move_wind_offset <- -1.59
}
#print(fta_tr)

# gather forecast hours
forecast_hours = as.integer(strftime(f$time, "%H"))
#print(forecast_hours)

# ta (measurements): min/max labels
# - before daytick_in_measurements
max_ta <- max(tmp_aggr$x[1:daytick_in_measurements], na.rm = TRUE)
min_ta <- min(tmp_aggr$x[1:daytick_in_measurements], na.rm = TRUE)
xoffset = xmt - .5
#cat("xoffset:", xoffset, "\n")
polygon(
    x = c(xoffset - .12, xoffset + .12, xoffset + .12, xoffset - .12),
    y = c(2.6 + move_wind_offset, 2.6 + move_wind_offset, 3 + move_wind_offset, 3 + move_wind_offset),
    border = NA, col = "white"
)
text(x = xoffset, y = 2.7 + move_wind_offset,
    round(min_ta, digits = 0), font = 2, cex = .7, adj = .5)
text(x = xoffset, y = 2.9 + move_wind_offset,
    round(max_ta, digits = 0), font = 2, cex = .7, adj = .5)

# hpa
ff$hpa_d <- ff$hpa - 1013.25 # deviation from normal pressure
fhpa_tr <- ff$hpa_d * .05 + 1.5

# dew/td
ftd_tr <- ff$dew * .05 + 1.5

# swr
# - 1367 W/m2 is the theoretical maximum of solar radiation, when the sun is
#   in the zenit.
# - 688 W/m2 = 1376 W/m2 * sin(60/360*pi) is the theoritical maximum of solar
#   radiation in Berlin (sun 60 degrees above the horizon on July 1).
# - 688 needs to be normalized to 0.75 instead of one, because only 0.75 of
#   space left: 917 = 688 * (1 / .75). Rounding to 950
ff$swr_tr <- ff$swr / 950
#print(ff$swr_tr)
fswr_tr <- ff$swr_tr * .25 + 3.25
#print(fswr_tr)

# clouds
# - take the maximum cloud cover
fcct_a <- rep(100, length(ff$ccl))
for (i in c(1:length(ff$ccl))) {
    fcct_a[i] <- (fcct_a[i] - max(c(ff$ccl[i], ff$ccm[i], ff$cch[i]),
        na.rm = TRUE)) * .01
}
#print(fcct_a)
fcct_a[fcct_a < 0] <- 0
fcct_p <- (1 - fcct_a) * .25 + 3.25
fcct_a[fcct_a < .25] <- .25
#print(fcct_a)
for (i in c(1:length(fcct_a)-1)) {
    polygon(
        x = c(ftime_tr[i], ftime_tr[i],
            ftime_tr[i + 1], ftime_tr[i + 1]),
        y = c(3.25, fcct_p[i], fcct_p[i], 3.25),
        col = rgb(0, 0, 0, fcct_a[i]),
        border = NA
    )
}

# rain
ffrain <- 3.2 - ff$tprc * .23
ffrain_a <- ff$pp
ffrain_a[ffrain_a < 0.5] <- 0.5
ffrain_a[ffrain_a > 0.8] <- 1
#print(ff$pp)
#print(ffrain_a)
for(i in c(1:length(ffrain)-1)) {
    polygon(
        x = c(ftime_tr[i], ftime_tr[i],
            ftime_tr[i + 1], ftime_tr[i + 1]),
        y = c(3.2, ffrain[i], ffrain[i], 3.2),
        col = rgb(0, 0, 0, ffrain_a[i]),
        border = NA
    )
}

# wind normalization
ffwin_a <- ff$win / 20.7
ffwin_a[ffwin_a > 1] <- 1
    # set Beaufort 8 (17.2-20.7 m/s) as max

# gust normalization
ffgust_a <- ff$gust / 13.9
ffgust_a[ffgust_a > 3] <- 3
    # set 3 times of Beaufort 7 (13.9-17.1 m/s) as max

# normalized gust warning threshold
# - roughly at Beaufort 7 when normalizations above are not changed (2023-12-22)
gusta_warning <- 1.223

# test
#gusta_warning <- 1

# north triangle (actualy pointing to south for wind coming from north)
tx <- c(-.09, 0, .09, 0)
ty <- c(.12, .08, .12, -.12)
rotate_x <- function(angle) {
    txa <- tx * cos(angle) - ty * sin(angle)

    return(txa)
}
rotate_y <- function(angle) {
    tya <- tx * sin(angle) + ty * cos(angle)

    return(tya)
}
to_radian <- function(angle) {
    # also reflects that rotate_x/y() rotate anti-clockwise
    b <- (360 - angle) / 360 * 2 * pi

    return(b)
}

plot_windrose <- function(xoffset, yoffset, gusta, wina) {
    if (gusta < .5) {
        gusta <- .5
    }
    polygon(
        x = txa * .75 + xoffset,
        y = tya + yoffset,
        col = "white",
        border = "white", lwd = ((gusta - .4) * 3) + 6
    )

    # plot everything black if a threshold for gust is reached
    if (gusta > gusta_warning) {
        wina <- 1
    }

    polygon(
        x = txa * .75 + xoffset,
        y = tya + yoffset,
        col = rgb(1 - wina, 1 - wina, 1 - wina),
        border = "black", lwd = (gusta - .4) * 3
    )
}

# print pressure graph before wind roses
par(new = TRUE)
plot(ftime_tr, fhpa_tr, lty = 2,
    lwd = 2, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)

# wind in next/current hour
win1a <- ffwin_a[1]
win1r <- to_radian(ff$dir[1])
txa <- rotate_x(win1r)
tya <- rotate_y(win1r)
#print(str(ffgust_a[1]))
plot_windrose(3.75, 4.05, ffgust_a[1], win1a)
text(x = 3.6, y = 4.0, round(ff$gust[1], digits = 0), font = 1,
    cex = 1.5, pos = 2)

# dayticks: forecast
xmts = day_ticks_in_plot[c(2, 3, 4)] 
for (xmt in xmts) {
    polygon(
        x = c(xmt, xmt),
        y = c(.5, 3.5),
        border = "#878787", lwd = 2, lty = 1
    )
}

# ta (measurements and forecast): min/max label of current day
mlength = length(tmp_aggr$x)
min_ta <- min(tmp_aggr$x[daytick_in_measurements:mlength], na.rm = TRUE)
max_ta <- max(tmp_aggr$x[daytick_in_measurements:mlength], na.rm = TRUE)
daytick_in_forecast = match(0, forecast_hours)
if (daytick_in_forecast == 1) {
    # next daytick, if a day just started
    daytick_in_forecast = 24
}
#cat("daytick_in_forecast:", daytick_in_forecast, "\n")
min_ta_f1 <- min(ff$tmp[1:daytick_in_forecast], na.rm = TRUE)
max_ta_f1 <- max(ff$tmp[1:daytick_in_forecast], na.rm = TRUE)
min_ta0 <- min(c(min_ta, min_ta_f1), na.rm = TRUE)
max_ta0 <- max(c(max_ta, max_ta_f1), na.rm = TRUE)

# ta0: current day
xoffset = xmts[2] - 1.5
#cat("xoffset:", xoffset, "\n")
polygon(
    x = c(xoffset - .12, xoffset + .12, xoffset + .12, xoffset - .12),
    y = c(2.6 + move_wind_offset, 2.6 + move_wind_offset, 3 + move_wind_offset, 3 + move_wind_offset),
    border = NA, col = "white"
)
text(x = xoffset, y = 2.7 + move_wind_offset,
    round(min_ta0, digits = 0), font = 2, cex = .7, adj = .5)
text(x = xoffset, y = 2.9 + move_wind_offset,
    round(max_ta0, digits = 0), font = 2, cex = .7, adj = .5)

# ta1: next day
nextdaytick_in_forecast = daytick_in_forecast + 24
min_ta1 <- min(ff$tmp[daytick_in_forecast:nextdaytick_in_forecast], na.rm = TRUE)
max_ta1 <- max(ff$tmp[daytick_in_forecast:nextdaytick_in_forecast], na.rm = TRUE)
xoffset = xmts[3] - 1.5
polygon(
    x = c(xoffset - .12, xoffset + .12, xoffset + .12, xoffset - .12),
    y = c(2.6 + move_wind_offset, 2.6 + move_wind_offset, 3 + move_wind_offset, 3 + move_wind_offset),
    border = NA, col = "white"
)
text(x = xoffset, y = 2.7 + move_wind_offset,
    round(min_ta1, digits = 0), font = 2, cex = .7, adj = .5)
text(x = xoffset, y = 2.9 + move_wind_offset,
    round(max_ta1, digits = 0), font = 2, cex = .7, adj = .5)

# ta2: day after tomorrow
dayaftertick_in_forecast = daytick_in_forecast + 48
min_ta2 <- min(ff$tmp[nextdaytick_in_forecast:dayaftertick_in_forecast], na.rm = TRUE)
max_ta2 <- max(ff$tmp[nextdaytick_in_forecast:dayaftertick_in_forecast], na.rm = TRUE)
xoffset = xmts[3] - .5
#cat("xoffset:", xoffset, "\n")
polygon(
    x = c(xoffset - .12, xoffset + .12, xoffset + .12, xoffset - .12),
    y = c(2.6 + move_wind_offset, 2.6 + move_wind_offset, 3 + move_wind_offset, 3 + move_wind_offset),
    border = NA, col = "white"
)
text(x = xoffset, y = 2.7 + move_wind_offset,
    round(min_ta2, digits = 0), font = 2, cex = .7, adj = .5)
text(x = xoffset, y = 2.9 + move_wind_offset,
    round(max_ta2, digits = 0), font = 2, cex = .7, adj = .5)

# ta3: last day
lastdaytick_in_forecast = length(ff$tmp)
min_ta3 <- min(ff$tmp[dayaftertick_in_forecast:lastdaytick_in_forecast], na.rm = TRUE)
max_ta3 <- max(ff$tmp[dayaftertick_in_forecast:lastdaytick_in_forecast], na.rm = TRUE)
xoffset = xmts[3] + .5
polygon(
    x = c(xoffset - .12, xoffset + .12, xoffset + .12, xoffset - .12),
    y = c(2.6 + move_wind_offset, 2.6 + move_wind_offset, 3 + move_wind_offset, 3 + move_wind_offset),
    border = NA, col = "white"
)
text(x = xoffset, y = 2.7 + move_wind_offset,
    round(min_ta3, digits = 0), font = 2, cex = .7, adj = .5)
text(x = xoffset, y = 2.9 + move_wind_offset,
    round(max_ta3, digits = 0), font = 2, cex = .7, adj = .5)

# max wind speed, gust and mean direction every 12 hours
# ta0: current day
if (current_hour < 12) {
    middaytick_in_forecast = match(12, forecast_hours)
    if (middaytick_in_forecast == 1) {
        # midnight (first daytick), if midday just started
        middaytick_in_forecast = 12
    }
    wina <- max(ffwin_a[1:middaytick_in_forecast], na.rm = TRUE)
    gusta <- max(ffgust_a[1:middaytick_in_forecast], na.rm = TRUE)
    winr <- to_radian(mean(ff$dir[1:middaytick_in_forecast], na.rm = TRUE))
    txa <- rotate_x(winr)
    tya <- rotate_y(winr)
    xoffset = xmts[2] - 1.75
    plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
    if (gusta > gusta_warning) {
        max_gust <- max(ff$gust[1:middaytick_in_forecast], na.rm = TRUE)
        text(x = xoffset, y = 2.75 + move_wind_offset,
            round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
    }
    wina <- max(ffwin_a[middaytick_in_forecast:daytick_in_forecast], na.rm = TRUE)
    gusta <- max(ffgust_a[middaytick_in_forecast:daytick_in_forecast], na.rm = TRUE)
    winr <- to_radian(mean(ff$dir[middaytick_in_forecast:daytick_in_forecast], na.rm = TRUE))
    txa <- rotate_x(winr)
    tya <- rotate_y(winr)
    xoffset = xmts[2] - 1.25
    plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
    if (gusta > gusta_warning) {
        max_gust <- max(ff$gust[middaytick_in_forecast:daytick_in_forecast], na.rm = TRUE)
        text(x = xoffset, y = 2.75 + move_wind_offset,
            round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
    }
} else {
    wina <- max(ffwin_a[1:daytick_in_forecast], na.rm = TRUE)
    gusta <- max(ffgust_a[1:daytick_in_forecast], na.rm = TRUE)
    winr <- to_radian(mean(ff$dir[1:daytick_in_forecast], na.rm = TRUE))
    txa <- rotate_x(winr)
    tya <- rotate_y(winr)
    xoffset = xmts[2] - 1.25
    plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
    if (gusta > gusta_warning) {
        max_gust <- max(ff$gust[1:daytick_in_forecast], na.rm = TRUE)
        text(x = xoffset, y = 2.75 + move_wind_offset,
            round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
    }
}

# ta1: next day
nextmiddaytick_in_forecast = daytick_in_forecast + 12
wina <- max(ffwin_a[daytick_in_forecast:nextmiddaytick_in_forecast], na.rm = TRUE)
gusta <- max(ffgust_a[daytick_in_forecast:nextmiddaytick_in_forecast], na.rm = TRUE)
winr <- to_radian(mean(ff$dir[daytick_in_forecast:nextmiddaytick_in_forecast], na.rm = TRUE))
txa <- rotate_x(winr)
tya <- rotate_y(winr)
xoffset = xmts[3] - 1.75
plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
if (gusta > gusta_warning) {
    max_gust <- max(ff$gust[daytick_in_forecast:nextmiddaytick_in_forecast], na.rm = TRUE)
    text(x = xoffset, y = 2.75 + move_wind_offset,
        round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
}
wina <- max(ffwin_a[nextmiddaytick_in_forecast:nextdaytick_in_forecast], na.rm = TRUE)
gusta <- max(ffgust_a[nextmiddaytick_in_forecast:nextdaytick_in_forecast], na.rm = TRUE)
winr <- to_radian(mean(ff$dir[nextmiddaytick_in_forecast:nextdaytick_in_forecast], na.rm = TRUE))
txa <- rotate_x(winr)
tya <- rotate_y(winr)
xoffset = xmts[3] - 1.25
plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
if (gusta > gusta_warning) {
    max_gust <- max(ff$gust[nextmiddaytick_in_forecast:nextdaytick_in_forecast], na.rm = TRUE)
    text(x = xoffset, y = 2.75 + move_wind_offset,
        round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
}

# ta2: day after tomorrow
middayaftertick_in_forecast = daytick_in_forecast + 36
wina <- max(ffwin_a[nextdaytick_in_forecast:middayaftertick_in_forecast], na.rm = TRUE)
gusta <- max(ffgust_a[nextdaytick_in_forecast:middayaftertick_in_forecast], na.rm = TRUE)
winr <- to_radian(mean(ff$dir[nextdaytick_in_forecast:middayaftertick_in_forecast], na.rm = TRUE))
txa <- rotate_x(winr)
tya <- rotate_y(winr)
xoffset = xmts[3] - .75
plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
if (gusta > gusta_warning) {
    max_gust <- max(ff$gust[nextdaytick_in_forecast:middayaftertick_in_forecast], na.rm = TRUE)
    text(x = xoffset, y = 2.75 + move_wind_offset,
        round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
}
wina <- max(ffwin_a[middayaftertick_in_forecast:dayaftertick_in_forecast], na.rm = TRUE)
gusta <- max(ffgust_a[middayaftertick_in_forecast:dayaftertick_in_forecast], na.rm = TRUE)
winr <- to_radian(mean(ff$dir[middayaftertick_in_forecast:dayaftertick_in_forecast], na.rm = TRUE))
txa <- rotate_x(winr)
tya <- rotate_y(winr)
xoffset = xmts[3] - .25
plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
if (gusta > gusta_warning) {
    max_gust <- max(ff$gust[middayaftertick_in_forecast:dayaftertick_in_forecast], na.rm = TRUE)
    text(x = xoffset, y = 2.75 + move_wind_offset,
        round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
}

# ta3: last day
flength = length(ffwin_a)
if (current_hour > 12) {
    lastmiddaytick_in_forecast = dayaftertick_in_forecast + 12
    wina <- max(ffwin_a[dayaftertick_in_forecast:lastmiddaytick_in_forecast], na.rm = TRUE)
    gusta <- max(ffgust_a[dayaftertick_in_forecast:lastmiddaytick_in_forecast], na.rm = TRUE)
    winr <- to_radian(mean(ff$dir[dayaftertick_in_forecast:lastmiddaytick_in_forecast], na.rm = TRUE))
    txa <- rotate_x(winr)
    tya <- rotate_y(winr)
    xoffset = xmts[3] + .25
    plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
    if (gusta > gusta_warning) {
        max_gust <- max(ff$gust[dayaftertick_in_forecast:lastmiddaytick_in_forecast], na.rm = TRUE)
        text(x = xoffset, y = 2.75 + move_wind_offset,
            round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
    }
    wina <- max(ffwin_a[lastmiddaytick_in_forecast:flength], na.rm = TRUE)
    gusta <- max(ffgust_a[lastmiddaytick_in_forecast:flength], na.rm = TRUE)
    winr <- to_radian(mean(ff$dir[lastmiddaytick_in_forecast:flength], na.rm = TRUE))
    txa <- rotate_x(winr)
    tya <- rotate_y(winr)
    xoffset = xmts[3] + .75
    plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
    if (gusta > gusta_warning) {
        max_gust <- max(ff$gust[lastmiddaytick_in_forecast:flength], na.rm = TRUE)
        text(x = xoffset, y = 2.75 + move_wind_offset,
            round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
    }
} else {
    wina <- max(ffwin_a[dayaftertick_in_forecast:flength], na.rm = TRUE)
    gusta <- max(ffgust_a[dayaftertick_in_forecast:flength], na.rm = TRUE)
    winr <- to_radian(mean(ff$dir[dayaftertick_in_forecast:flength], na.rm = TRUE))
    txa <- rotate_x(winr)
    tya <- rotate_y(winr)
    xoffset = xmts[3] + .25
    plot_windrose(xoffset, 2.8 + move_wind_offset, gusta, wina)
    if (gusta > gusta_warning) {
        max_gust <- max(ff$gust[dayaftertick_in_forecast:flength], na.rm = TRUE)
        text(x = xoffset, y = 2.75 + move_wind_offset,
            round(max_gust, digits = 0), font = 3, cex = .9, pos = 1)
    }
}

# thunder: max propability of thunder within the next 12 hours
ffth <- max(ff$thunder[1:12], na.rm = TRUE) * 5
ffth[ffth > 1] <- 1
#print(ffth)
       # 1     2     3    4    5    6     7
thx <- c(-.07, -.03, .05, .02, .1,  -.05, -.01)
thy <- c(0,    .12,   .12,  .05, .05, -.1,  0)
polygon(
    x = thx + 3.75,
    y = thy + 4.45,
    col = rgb(1 - ffth, 1 - ffth, 1 - ffth, 1),
    border = rgb(1 - ffth, 1 - ffth, 1 - ffth, 1)
)

# plot forecast
par(new = TRUE)
plot(ftime_tr, fswr_tr,
    lwd = 1, type = "l", lty = 1,
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)

par(new = TRUE)
plot(ftime_tr, ftd_tr, col = "white",
    lwd = 3, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(ftime_tr, fta_tr, col = "white",
    lwd = 11, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(ftime_tr, ftd_tr,
    lwd = 1.5, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)
par(new = TRUE)
plot(ftime_tr, fta_tr,
    lwd = 5, type = "l",
    axes = FALSE, ylab = "", xlab = "", ylim = yrange, xlim = xrange
)

#print(str(ff))
#dev.off()
