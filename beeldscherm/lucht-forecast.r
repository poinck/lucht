#!/usr/bin/Rscript

require(xml2)
require(data.table)
require(jsonlite)

# load configuration
rc <- read.csv(file = "~/.luchtrc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
rc_table <- data.table(rc, key = "key")
lucht_path <- rc_table["lucht_path"]$value
cat("lucht_path =", lucht_path, "\n")
lucht_webdir <- rc_table["lucht_webdir"]$value
cat("lucht_webdir =", lucht_webdir, "\n")

mm <- read_xml(paste0(lucht_path, "station.kml"))

daysinh = 97

time <- xml_find_all(mm, "//dwd:TimeStep")
times <- as.POSIXct(xml_text(xml_contents(time)),
    format = "%Y-%m-%dT%H:%M", tz = "UTC")
times <- times[2:daysinh]
#str(times)

value <- xml_find_all(mm, "//dwd:value")
values <- xml_text(xml_contents(value))

pppp <- as.numeric(unlist(strsplit(values[1], "\\s+"))) / 100
pppp <- pppp[2:daysinh]
#str(pppp)

ttt <- as.numeric(unlist(strsplit(values[4],  "\\s+"))) - 273.15
ttt <- ttt[2:daysinh]
#str(ttt)

#td <- as.numeric(unlist(strsplit(values[6], c("     ")))) - 273.15
td <- as.numeric(unlist(strsplit(values[6], "\\s+"))) - 273.15
td <- td[2:daysinh]
#str(td)

# wind direction
dd <- as.numeric(unlist(strsplit(values[12], "\\s+")))
dd <- dd[2:daysinh]
#str(dd)

# wind speed
ff <- as.numeric(unlist(strsplit(values[14], "\\s+")))
ff <- ff[2:daysinh]
#str(ff)

# wind gust
fx1 <- as.numeric(unlist(strsplit(values[16], "\\s+")))
fx1 <- fx1[2:daysinh]

# cloud cover
#neff <- as.numeric(unlist(strsplit(values[26], "\\s+")))
#neff <- neff[2:97]
#str(neff)

# low clouds
nl <- as.numeric(unlist(strsplit(values[30], "\\s+")))
nl <- nl[2:daysinh]

# high clouds
nh <- as.numeric(unlist(strsplit(values[28], "\\s+")))
nh <- nh[2:daysinh]

# mid clouds
nm <- as.numeric(unlist(strsplit(values[29], "\\s+")))
nm <- nm[2:daysinh]

# thunder
wwt <- as.numeric(unlist(strsplit(values[54], "\\s+")))
wwt <- wwt[2:daysinh]
wwt <- wwt * .01
#print(wwt)

# total precipitation consistent with significant weather (kg/m2, rain or snow)
tprc <- as.numeric(unlist(strsplit(values[71], "\\s+")))
tprc <- tprc[2:daysinh]

# propability of percipitation
pp <- as.numeric(unlist(strsplit(values[41], "\\s+")))
pp <- pp[2:daysinh]
#str(rr1c)
#rr1c[rr1c < 34] <- 0
pp <- pp * .01
#print(pp)
#print(values)

# global irradiance (kJ/m2)
rad1h <- as.numeric(unlist(strsplit(values[106], "\\s+")))
rad1h <- rad1h[2:daysinh]
# to W/m2
rad1h <- rad1h / 3.6
#str(rad1h)

rh <- 100 * (exp((17.625 * td)/(243.04 + td)) / exp((17.625 * ttt) / (243.04 + ttt)))

f <- data.frame(
    time = times,
    tmp = ttt,
    dew = td,
    hpa = pppp,
    #tcc = neff,
    ccl = nl,
    cch = nh,
    ccm = nm,
    swr = rad1h,
    dir = dd,
    win = ff,
    gust = fx1,
    tprc = tprc,
    pp = pp,
    hum = rh,
    thunder = wwt
)
#str(f)
save(f, file = paste0(lucht_path, "forecast.rda"))
#write.csv(f, file = paste0(lucht_webdir, "forecast.csv"), row.names = FALSE)
f_json <- toJSON(f, pretty = TRUE, na = "null")
write(f_json, paste0(lucht_webdir, "forecast.json"))
#cat("forecast saved\n")
