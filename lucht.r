#!/usr/bin/Rscript

require(data.table)

history_filename <- "lucht.png"

# load configuration
rc <- read.csv(file = "~/.luchtrc", sep = "=", header = FALSE,
    col.names = c("key", "value"))
rc_table <- data.table(rc, key = "key")
data_path <- rc_table["data_path"]$value
lucht_path <- rc_table["lucht_path"]$value
box_height <- as.integer(rc_table["station_height"]$value)
cat("data_path =", data_path, "\n")
cat("lucht_path =", lucht_path, "\n")
cat("box_height =", box_height, "\n")

# get arguments/parameters
args <- commandArgs(trailingOnly = TRUE)
#print(args)
tolino = FALSE
if (length(args) > 0) {
    if (args[1] == "tolino") {
        history_filename <- "lucht_tolino.png"
        tolino <- TRUE
    }
}

calculate_dewpoint <- function(tmp, hum) {
    dew <- NA

    if (!is.na(hum) && !is.na(tmp)) {
        # derived from Magnus formula using Sonntag (1990) coefficients
        c <- 243.12
        b <- 17.62
        dew <- c * ((log(hum / 100) + ((b * tmp)/(c + tmp))) / (b - log(hum /   100) - ((b * tmp)/(c + tmp))))
    }

    return(dew)
}

calculate_dews <- function(tmps, hums) {
    l <- length(tmps)
    dews <- rep(NA, l)

    for (i in c(1:l)) {
        dews[i] <- calculate_dewpoint(tmps[i], hums[i])
    }

    return(dews)
}

# NOTUSED:
calculate_corrected_pm <- function(pm, hum) {
    # relative humidity correction of particulate matter
    # based on paper: Antonio et al. 2018 "sensors (basel)"
    r <- NA

    #cat("hum:", hum, "\n")
    if (!is.na(hum)) {
        if (hum == 100) {
            # avoid devision by zero
            hum <- 99.99
        }

        kappa <- .3     # efflorescence point of compound that was measured
                        # (unkown: so .4 is an assumption)
        aw <- hum / 100 # water activity RH/100

        C <- 1 + ((kappa / 1.65) / (-1 + (1 / aw)))
        r <- pm / C
    }

    return(r)
}

calculate_reduced_air_pressure <- function(tmp, hum, hpa, h) {
    # based on equation of DWD (Deutscher Wetterdienst)
    r <- NA

    g0 <- 9.80665   # acceleration caused by earth gravity (m/s²)
    Ch <- 0.12      # change of water wapor pressure (K/hPa)
    Rair <- 287.05  # gas constant of dry air (m²/s²K)
    a <- 0.0065     # vertical temperature gradient (K/m)

    Th <- tmp + 273.15
    E <- hpa * (hum / 100)
    r <- hpa * exp(g0 / (Rair * (Th + Ch * E + a * (h / 2))) * h)

    return(r)
}

reduce_air_pressure <- function(rt, tmp, hum, hpa, h) {
    l <- length(rt)
    r <- rep(NA, l)

    for (i in c(1:l)) {
        if (!is.na(tmp[i]) && !is.na(hum[i]) && !is.na(hpa[i])) {
            r[i] <- calculate_reduced_air_pressure(tmp[i], hum[i], hpa[i], h)
        }
    }

    return(r)
}

# get current date and time
now <- as.POSIXct(Sys.time(), tz = "UTC")
#print(now)

history_png <- paste(lucht_path, history_filename, sep = "")
#resw = 480
#resh = 854
#pointsize = 11
daysins = 345600 # 4 days
resw = 607
#resh = 1000
resh = 985
pointsize = 14
#daysins = 259200 # 3 days
if (tolino) {
    resw = 749
    resh = 906
    pointsize = 12
    daysins = 432000 # 5 days
}
png(filename = history_png,
    width = resw, height = resh,
    units = "px",
    pointsize = pointsize,
    res = 150,
    family = "Atkinson Hyperlegible",
    bg = "#dddddd", type = "cairo"
)

# read forecast: ICON (preprocessed from station kml)
load(paste0(lucht_path, "forecast.rda"))

# read fetched data
lucht_file <- paste(data_path, "lucht_5d.csv", sep = "")
lucht <- read.csv(lucht_file, skipNul = TRUE)
names(lucht) <- c(
    "time",
    "pm1",
    "pm4",
    "sps_err",
    "ta1",
    "hum1",
    "sht1_err",
    "ta2",
    "hum2",
    "sht2_err",
    "ta",
    "hum",
    "sht_err",
    "t_logger",
    "p",
    "icp_err",
    "not_used1",
    "not_used2",
    "not_used3"
)
lucht$time <- as.POSIXct(strptime(lucht$time, "%Y-%m-%dT%H:%M+00:00",
    tz = "UTC"))
#str(lucht)
#tmp <- tmp[dim(tmp)[1]:1, ]  # reverse order

start_end_seq <- seq(
    from = round(as.POSIXct(lucht$time[length(lucht$ta)],
        format = "%Y-%m-%dT%H:00:00",
        tz = "UTC"), units = "hours") - daysins,
    to = lucht$time[length(lucht$ta)],
    by = "3600 sec")
#print(start_end_seq)
tmp_time_aggr <- cut(x = lucht$time, breaks = start_end_seq)
hum_time_aggr <- cut(x = lucht$time, breaks = start_end_seq)
pm1_time_aggr <- cut(x = lucht$time, breaks = start_end_seq)
hpa_time_aggr <- cut(x = lucht$time, breaks = start_end_seq)
#length(tmp_time_aggr)
#length(dew_time_aggr)
tmp_aggr <- aggregate(lucht$ta, FUN = mean,
    by = list(time_aggr = tmp_time_aggr), na.rm = TRUE)

hum_aggr <- aggregate(lucht$hum, FUN = mean,
    by = list(time_aggr = hum_time_aggr), na.rm = TRUE)
#print(hum_aggr$x)
hum_aggr$x <- hum_aggr$x

# fix unplausible temperatures
tmp_aggr$x[tmp_aggr$x < -80] <- NA
    # NOTE: last values of vector can now be NA, which disables display of
    # legend (this is ok).

dew_aggr_x <- calculate_dews(tmp_aggr$x, hum_aggr$x)
#print(tmp_aggr$x)
#print(hum_aggr$x)
pm1_aggr <- aggregate(lucht$pm1, FUN = mean,
    by = list(time_aggr = pm1_time_aggr), na.rm = TRUE)
pm1_mina <- aggregate(lucht$pm1, FUN = min,
    by = list(time_aggr = pm1_time_aggr), na.rm = TRUE)
pm1_maxa <- aggregate(lucht$pm1, FUN = max,
    by = list(time_aggr = pm1_time_aggr), na.rm = TRUE)
#print(pm1_mina)
#print(pm1_maxa)
#print(pm1_aggr)

hpa_aggr <- aggregate(lucht$p, FUN = mean,
    by = list(time_aggr = hpa_time_aggr), na.rm = TRUE)
#str(hpa_aggr$x)
hpa_aggr$x <- hpa_aggr$x# * .01
hpa_aggr$x <- reduce_air_pressure(hpa_aggr$time_aggr, tmp_aggr$x, hum_aggr$x,
    hpa_aggr$x, box_height)
hpa_aggr$x <- hpa_aggr$x - 1013.25 # deviation from normal pressure
hpa_aggr$x[tmp_aggr$x < -80] <- NA
    # remove pressure values where temperature makes no sense
#str(hpa_aggr$x)
#str(dew_aggr$x)
#print(dew_aggr_x)

days <- strftime(tmp_aggr$time_aggr, "%Y-%m-%d", tz = "UTC")
days <- days[!duplicated(days)]
#print(days)

par(mfrow = c(2, 1), xpd = NA, mar = c(0, 0, 0, 0),
    xaxs = "i"
    #yaxs = "i"
)

tmp_range = range(c(-15, 45))
time_range = range(c(
    start_end_seq[1], start_end_seq[length(start_end_seq)-1]
))

ndcol = "#aaaaaa"
if (tolino) {
    ndcol = "#787878"
}

plot(as.POSIXct(start_end_seq, tz = "UTC"),
    panel.first = {
        abline(h = 0, lwd = 1, lty = 3, col = ndcol)
        abline(v = as.POSIXct(days, tz = "UTC"), lwd = 1, lty = 3,
        col = ndcol, xpd = TRUE)
    },
    rep(NA, length(start_end_seq)), type = "l", xlab = "",
    ylim = tmp_range, xlim = time_range,
    axes = FALSE, col = "lightskyblue2", lwd = 1.5)
par(new = TRUE)
minp <- 0
maxp <- 25
p_alpha <- (pm1_aggr$x - minp) / (maxp - minp)
p_alpha[p_alpha > 1] <- 1
p_alpha[p_alpha < .25] <- .25
par(new = TRUE)
plot(as.POSIXct(hpa_aggr$time_aggr, tz = "UTC"), hpa_aggr$x,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = time_range,
    col = "mediumseagreen", lwd = 1.5
)

p25col = rgb(0.545, 0.450, 0.333, p_alpha)
if (tolino) {
    p25col = rgb(0.212, 0.117, 0, p_alpha)
}

for(i in c(2:length(pm1_mina$time_aggr)-1)) {
    polygon(
        x = c(as.POSIXct(pm1_mina$time_aggr[i], tz = "UTC"),
            as.POSIXct(pm1_maxa$time_aggr[i], tz = "UTC"),
            as.POSIXct(pm1_maxa$time_aggr[i + 1], tz = "UTC"),
            as.POSIXct(pm1_mina$time_aggr[i + 1], tz = "UTC")),
        y = c(pm1_mina$x[i] - 1, pm1_mina$x[i],
            pm1_mina$x[i], pm1_mina$x[i] - 1),
        col = p25col[i], # burlywood4 (darker on tolino)
        border = NA, angle = 0, density = 35, lty = 3
    )
}
par(new = TRUE)
plot(as.POSIXct(tmp_aggr$time_aggr, tz = "UTC"), tmp_aggr$x,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = time_range,
    col = "#dddddd", lwd = 7)
par(new = TRUE)
plot(as.POSIXct(tmp_aggr$time_aggr, tz = "UTC"), dew_aggr_x,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = time_range,
    col = "#dddddd", lwd = 2.5)
par(new = TRUE)
plot(as.POSIXct(tmp_aggr$time_aggr, tz = "UTC"), dew_aggr_x,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = time_range,
    col = "hotpink3", lwd = 1)

par(new = TRUE)
plot(as.POSIXct(tmp_aggr$time_aggr, tz = "UTC"), tmp_aggr$x,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = time_range,
    col = "black", lwd = 3)

axcol = "#aaaaaa"
if (tolino) {
    axcol = "#787878"
}

axis(side = 2, col.axis = axcol, line = -2, tick = FALSE)
axis(side = 3, col.axis = axcol, line = -2, tick = FALSE,
    at = as.POSIXct(days, tz = "UTC") + 60 * 60 * 12,
    labels = strftime(days, "%a", tz = "UTC"))
p25_label <- round(pm1_aggr$x[sum(!is.na(pm1_aggr$x))], digits = 0)
#p25_col <- rgb(.3, .3, .7)
p25_col <- "burlywood4"
measure_time_formatted <- strftime(tmp_aggr$time_aggr[length(tmp_aggr$x)],
    "%Y%m%d %H:%Mz", tz = "UTC")

lastdayi = 4
lastdayi1 = 3
if (tolino) {
    lastdayi = 5
    lastdayi1 = 4
}

text(x = as.POSIXct(days[2], tz = "UTC"), y = 40, pos = 4,
    labels = measure_time_formatted, font = 2,
    col = axcol
)
text(x = as.POSIXct(days[lastdayi], tz = "UTC") + 60 * 60 * 8, y = 40,
    labels = paste0(
        round(tmp_aggr$x[length(tmp_aggr$x)], digits = 0), "°C"
    ), font = 2,
    col = "black", pos = 2
)
text(x = as.POSIXct(days[lastdayi], tz = "UTC") + 60 * 60 * 8, y = 40,
    labels = paste0(
        round(dew_aggr_x[length(dew_aggr_x)], digits = 0), "°C"
    ), font = 2,
    col = "hotpink3", pos = 4
)
text(x = as.POSIXct(days[lastdayi], tz = "UTC") + 60 * 60 * 8, y = 37.5,
    labels = paste0(
        round(hum_aggr$x[length(hum_aggr$x)], digits = 0), "%"
    ), font = 2,
    col = "hotpink3", pos = 4, cex = .7
)

text(x = as.POSIXct(days[lastdayi1], tz = "UTC") + 60 * 60 * 10, y = -15,
    labels = paste0(p25_label, " µg/m³"), font = 2,
    col = p25_col, pos = 4
)

text(x = as.POSIXct(days[lastdayi], tz = "UTC") + 60 * 60 * 8, y = -15,
    labels = paste0(
        round(hpa_aggr$x[length(hpa_aggr$x)], digits = 0) + 1013, " hPa"
    ), font = 2,
    col = "seagreen", pos = 4
)

# add 1 day
newday <- as.POSIXct(days[length(days)], tz = "UTC") + (60 * 60 * 24)
days <- c(days, strftime(newday, "%Y-%m-%d", tz = "UTC"))

#str(tmp_aggr)
#str(days)

for (l in 1:length(days)-1) {
    fs <- subset(tmp_aggr,
        as.POSIXct(time_aggr, tz = "UTC") > as.POSIXct(days[l], tz = "UTC"))
    fs <- subset(fs,
        as.POSIXct(time_aggr, tz = "UTC") < as.POSIXct(days[l+1], tz = "UTC"))
    #str(fs)
    #print(fs$time_aggr)

    # max temp
    oldw <- getOption("warn")
    options(warn = -1)
    max1_tmp <- max(fs$x, na.rm = TRUE)
    max1_tmp_y <- max(fs$x, na.rm = TRUE) + 2
    max1_tmp_x <- which.max(fs$x)
    options(warn = oldw)
    if (length(max1_tmp_x) > 0) {
        # realign
        if (max1_tmp > 10) {
            if (max1_tmp_x < 9) {
                max1_tmp_x <- 9
            }
        } else {
            if (max1_tmp_x < 6) {
                max1_tmp_x <- 6
            }
        }
    }
    #print(fs$x)
    #cat("max1_tmp_x =", max1_tmp_x, "\n")
    text(x = as.POSIXct(fs$time_aggr[max1_tmp_x], tz = "UTC") + 7200,
        y = max1_tmp_y, pos = 2,
        cex = .6, labels = round(max1_tmp, digits = 1), font = 1,
        col = "black"
    )
    #print(max1_tmp)

    # min temp
    oldw <- getOption("warn")
    options(warn = -1)
    min1_tmp <- min(fs$x, na.rm = TRUE)
    min1_tmp_y <- min(fs$x, na.rm = TRUE) - 2
    min1_tmp_x <- which.min(fs$x)
    options(warn = oldw)
    if (length(min1_tmp_x) > 0) {
        # realign
        if (min1_tmp < -10) {
            if (min1_tmp_x < 11) {
                min1_tmp_x <- 11
            }
        } else if (min1_tmp < 0) {
            if (min1_tmp_x < 8) {
                min1_tmp_x <- 8
            }
        } else {
            if (min1_tmp_x < 6) {
                min1_tmp_x <- 6
            }
        }
    }
    #cat("min1_tmp_x =", min1_tmp_x, "\n")
    text(x = as.POSIXct(fs$time_aggr[min1_tmp_x], tz = "UTC") + 7200,
        y = min1_tmp_y, pos = 2,
        cex = .6, labels = round(min1_tmp, digits = 1), font = 1,
        col = "black"
    )
    #cat("next day\n")
}


days <- strftime(f$time, "%Y-%m-%d", tz = "UTC")
days <- days[!duplicated(days)]

xrange_time = range(c(f$time[1], f$time[length(f$time)]))

plot(as.POSIXct(f$time, tz = "UTC"), f$hpa - 1013.25,
    panel.first = {
        abline(h = 0, lwd = 1, lty = 3, col = ndcol)
        abline(v = now, lwd = 1, lty = 1, col = ndcol, xpd = TRUE)
        abline(v = as.POSIXct(days, tz = "UTC"), lwd = 1, lty = 3,
            col = ndcol, xpd = TRUE)
    },
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = xrange_time,
    col = "mediumseagreen", lwd = 1.5
)

#print(days)

par(new = TRUE)
plot(as.POSIXct(f$time, tz = "UTC"), f$swr / 136.7 + 30,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = xrange_time,
    col = "white", lwd = 1
)

# test
#f$gust[61] = 25
#f$gust[62] = 34
#f$gust[63] = 37
#f$gust[64] = 24

# gust
# - only print >= Beaufort 8 (20.7 m/s, Stürmischer Wind):
#   .47 = 17.2 / 36.9 (max: Orkan)
gust_alpha <- f$gust / 36.9
#print(f$gust)
gust_alpha <- ifelse(is.na(gust_alpha), 0, gust_alpha)
gust_alpha <- ifelse(gust_alpha > .46, gust_alpha, 0)
gust_alpha <- ifelse(gust_alpha > 1, 1, gust_alpha)
gustcol = "gold3"
wincol = "gold2"
gustcols = rgb(.933, .788, 0, gust_alpha)
if (tolino) {
    wincol = "black"
    gustcol = "#545454"
    gustcols = rgb(.34, .34, .34, gust_alpha)
}
#print(gustcols)
#print(gust_alpha)
segments(x0 = f$time, y0 = f$gust,
    x1 = f$time, y1 = f$win,
    #col = rgb(.933, .788, 0, gust_alpha),
    col = gustcols,
    lty = 1, lwd = 2.5
)

#cat("HERE\n")
par(new = TRUE)
plot(as.POSIXct(f$time, tz = "UTC"), f$win,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = xrange_time,
    col = wincol, lwd = 1.5, lty = 3
)

# low clouds
f$clouds <- f$ccl * .05 + 30
clouds_alpha <- 1-f$ccl*.01
clouds_alpha <- ifelse(is.na(clouds_alpha), 0, clouds_alpha)
clouds_alpha <- ifelse(clouds_alpha > .34, clouds_alpha, .34)
for(i in c(1:length(f$clouds)-1)) {
    polygon(
        x = c(f$time[i], f$time[i],
            f$time[i + 1], f$time[i + 1]),
        y = c(30, f$clouds[i], f$clouds[i], 30),
        col = rgb(1, 1, 1, clouds_alpha[i]),
        border = NA
    )
}

# high clouds
f$clouds <- f$cch * .025 + 40
clouds_alpha <- 1-f$cch*.01
clouds_alpha <- ifelse(is.na(clouds_alpha), 0, clouds_alpha)
clouds_alpha <- ifelse(clouds_alpha > .34, clouds_alpha, .34)
for(i in c(1:length(f$clouds)-1)) {
    polygon(
        x = c(f$time[i], f$time[i],
            f$time[i + 1], f$time[i + 1]),
        y = c(40, f$clouds[i], f$clouds[i], 40),
        col = rgb(1, 1, 1, clouds_alpha[i]),
        border = NA
    )
}

# mid clouds
f$clouds <- f$ccm * -.05 + 40
clouds_alpha <- 1-f$ccm*.01
clouds_alpha <- ifelse(is.na(clouds_alpha), 0, clouds_alpha)
clouds_alpha <- ifelse(clouds_alpha > .34, clouds_alpha, .34)
for(i in c(1:length(f$clouds)-1)) {
    polygon(
        x = c(f$time[i], f$time[i],
            f$time[i + 1], f$time[i + 1]),
        y = c(40, f$clouds[i], f$clouds[i], 40),
        col = rgb(1, 1, 1, clouds_alpha[i]),
        border = NA
    )
}


f$rain <- 30 - (f$pp * 5)
rain_alpha <- f$pp
raincol = rgb(.3, .3, .7, rain_alpha)
raincoll = rgb(.3, .3, .7)
if (tolino) {
    raincol = rgb(0, 0, .4, rain_alpha)
    raincoll = rgb(0, 0, .4)
}
#print(rain_alpha)
#print(f$time[i])
#print(f$rain[i])
segments(x0 = f$time, y0 = f$rain,
    x1 = f$time, y1 = f$rain,
    col = raincol,
    lty = 3, lwd = 2.5
)

# test
#f$tprc[48] <- .2
#f$tprc[49] <- .8
#f$tprc[50] <- 1.2
#f$tprc[51] <- 1

f$rainc <- 30 - f$tprc
for(i in c(1:length(f$rainc)-1)) {
    polygon(
        x = c(f$time[i], f$time[i],
            f$time[i + 1], f$time[i + 1]),
        y = c(30, f$rainc[i], f$rainc[i], 30),
        col = rgb(.3, .3, .7, rain_alpha[i]), border = NA
    )
}

par(new = TRUE)
plot(as.POSIXct(f$time, tz = "UTC"), f$tmp,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = xrange_time,
    col = "#dddddd", lwd = 7
)

# test
#f$thunder[61] <- .32
#f$thunder[62] <- .67
#f$thunder[63] <- .54

# thunder
thunder_alpha <- .23 + f$thunder
thunder_alpha <- ifelse(thunder_alpha > 1, 1, thunder_alpha)
thunder_alpha <- ifelse(thunder_alpha == .23, 0, thunder_alpha)
f$thunder <- f$thunder * 10 #+ f$win
#print(f$thunder)
segments(x0 = f$time + 1800, y0 = 35 - (f$thunder / 2),
    x1 = f$time + 1800, y1 = 35 + (f$thunder / 2),
    #col = rgb(.251, .878, .816, thunder_alpha),
    #col = rgb(0, .898, .933, thunder_alpha),
    col = rgb(0, 0, 0, thunder_alpha),
    lty = "solid", lwd = 2.5, lend = 0
)

par(new = TRUE)
plot(as.POSIXct(f$time, tz = "UTC"), f$dew,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = xrange_time,
    col = "#dddddd", lwd = 2.5
)

par(new = TRUE)
plot(as.POSIXct(f$time, tz = "UTC"), f$dew,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = xrange_time,
    col = "hotpink3", lwd = 1
)

par(new = TRUE)
plot(as.POSIXct(f$time, tz = "UTC"), f$tmp,
    type = "l", xlab = "", ylim = tmp_range, axes = FALSE,
    xlim = xrange_time,
    col = "black", lwd = 3
)

axis(side = 4, col.axis = axcol, line = -2, tick = FALSE)
axis(side = 1, col.axis = axcol, line = -2, tick = FALSE,
    at = as.POSIXct(days, tz = "UTC") + 60 * 60 * 12,
    labels = strftime(days, "%a", tz = "UTC"))

#print(f$time)
now_index <- 2
if (now > as.POSIXct(f$time[1], tz = "UTC") &&
    now < as.POSIXct(f$time[length(f$time)], tz = "UTC")) {
    #cat(" forecast has meaning, going on ..")
    for (i in 1:length(f$time)) {
        if (now < as.POSIXct(f$time[i], tz = "UTC")) {
            break
        }
    }
    #cat(i, "\n")
    now_index <- i
}
if (now_index-1 < 1) {
    now_index <- now_index + 1
    #cat(" early bird? ..")
}
now_formatted <- strftime(now, "%H:%Mz", tz = "UTC")

l_time <- now_formatted
x_hum <- as.POSIXct(f$time[now_index+3])
x_time <- as.POSIXct(f$time[now_index])
y_ftmp_text <- max(f$tmp[now_index-1:now_index+12], na.rm = TRUE) + 5
l_tmp <- round(f$tmp[now_index], digits = 0)
l_pp <- round(f$pp[now_index] * 100, digits = 0)
l_tprc <- round(f$tprc[now_index], digits = 1)
l_win <- round(f$win[now_index], digits = 0)
l_gust <- round(f$gust[now_index], digits = 0)
l_dew <- round(f$dew[now_index], digits = 0)
l_hum <- round(f$hum[now_index], digits = 0)
l_hpa <- round(f$hpa[now_index] - 1013.25, digits = 0)

# add 1 day
newday <- as.POSIXct(days[length(days)], tz = "UTC") + (60 * 60 * 24)
days <- c(days, strftime(newday, "%Y-%m-%d", tz = "UTC"))

#print(x_time)
for (l in 1:length(days)-1) {
    fs <- subset(f, time > days[l])
    fs <- subset(fs, time < days[l+1])
    #print(fs$time)

    # max gust
    oldw <- getOption("warn")
    options(warn = -1)
    max1_gust <- max(fs$gust, na.rm = TRUE)
    max1_gust_y <- max(fs$gust, na.rm = TRUE) + 2
    max1_gust_x <- which.max(fs$gust)
    options(warn = oldw)
    if (length(max1_gust_x) > 0) {
        # realign
        if (max1_gust_x < 9) {
            max1_gust_x <- 9
        }
    }
    if (max1_gust > 16.5) {
        text(x = fs$time[max1_gust_x] + 7200,
            y = max1_gust_y, pos = 2,
            cex = .6, labels = round(max1_gust, digits = 1), font = 2,
            col = gustcol
        )
        #print(max1_gust)
        #print(max1_gust_y)
        #print(max1_gust_x)
        #print(as.POSIXct(days[d], tz = "UTC") + max1_gust_x)
        #print(x_time)
    }

    # max temp
    oldw <- getOption("warn")
    options(warn = -1)
    max1_tmp <- max(fs$tmp, na.rm = TRUE)
    max1_tmp_y <- max(fs$tmp, na.rm = TRUE) + 2
    max1_tmp_x <- which.max(fs$tmp)
    options(warn = oldw)
    if (length(max1_tmp_x) > 0) {
        # realign
        if (max1_tmp > 10) {
            if (max1_tmp_x < 9) {
                max1_tmp_x <- 9
            }
        } else {
            if (max1_tmp_x < 6) {
                max1_tmp_x <- 6
            }
        }
    }
    #print(fs$tmp)
    #cat("max1_tmp_x =", max1_tmp_x, "\n")
    text(x = fs$time[max1_tmp_x] + 7200,
        y = max1_tmp_y, pos = 2,
        cex = .6, labels = round(max1_tmp, digits = 1), font = 1,
        col = "black"
    )
    #print(max1_tmp)

    # min temp
    oldw <- getOption("warn")
    options(warn = -1)
    min1_tmp <- min(fs$tmp, na.rm = TRUE)
    min1_tmp_y <- min(fs$tmp, na.rm = TRUE) - 2
    min1_tmp_x <- which.min(fs$tmp)
    options(warn = oldw)
    if (length(min1_tmp_x) > 0) {
        # realign
        if (min1_tmp < -10) {
            if (min1_tmp_x < 11) {
                min1_tmp_x <- 11
            }
        } else if (min1_tmp < 0) {
            if (min1_tmp_x < 8) {
                min1_tmp_x <- 8
            }
        } else {
            if (min1_tmp_x < 6) {
                min1_tmp_x <- 6
            }
        }
    }
    #cat("min1_tmp_x =", min1_tmp_x, "\n")
    text(x = fs$time[min1_tmp_x] + 7200,
        y = min1_tmp_y, pos = 2,
        cex = .6, labels = round(min1_tmp, digits = 1), font = 1,
        col = "black"
    )
    #cat("next day\n")
}

text(x = x_time, y = 45, pos = 4,
    labels = l_time, font = 2,
    col = axcol
)
text(x = x_time + 60 * 60 * 32, y = 45, pos = 2,
    labels = paste0(l_tmp, "°C"), font = 2,
    col = "black"
)

text(x = x_time, y = -10, pos = 4,
    labels = paste0(l_tprc, " mm"), font = 2,
    col = raincoll
)
text(x = x_time, y = -7.5, pos = 4,
    labels = paste0(l_pp, "%"), font = 2,
    cex = .7, col = raincoll
)

text(x = x_time + 60 * 60 * 40, y = -7.5, pos = 2,
    cex = .7, labels = paste0(l_gust, " m/s"), font = 2,
    col = gustcol
)
text(x = x_time + 60 * 60 * 40, y = -10, pos = 2,
    labels = paste0(l_win, " m/s"), font = 2,
    col = wincol
)
text(x = x_time + 60 * 60 * 32, y = 45, pos = 4,
    labels = paste0(l_dew, "°C"), font = 2,
    col = "hotpink3"
)
text(x = x_time + 60 * 60 * 32, y = 42.5, pos = 4,
    labels = paste0(l_hum, "%"), font = 2,
    col = "hotpink3", cex = .7
)
text(x = x_time + 60 * 60 * 46, y = -10, pos = 4,
    labels = paste0(1013 + l_hpa, " hPa"), font = 2,
    col = "seagreen"
)

#print(f_dwd$tmp[1:24])

#dev.off()
