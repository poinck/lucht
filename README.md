# Readme: lucht

"lucht" is the successor of [luft](https://gitlab.com/poinck/luft). It provides code for the Arduino platform to gather measurements from a custom weather station. code for a Raspberry Pi (or different appropiate Linux platform) to log and plot those measurements and configuration to setup an ESP8266 via esphome.

## Features

- Read measurements from Sensirion SHT85 temperature and humidity sensor
- Read atmospheric pressure
- Read particulate matter from Sensirion SPS30
- Log measurements with error codes as CSV
- Plot measurements of past 4 days
- Plot forecast from DWD (Deutscher Wetterdienst) for the next 4 days _(configuration: see second last chapter)_
- Provide the last measurement as JSON usable for integration in to other systems like "Home-Assistant"
- Plot a 24 hour plot suitable for e-ink displays
- Push current 24-hour-plot to a different system via custom remote copy command

_Planned features:_
- Push data to public databases like "https://luftdaten.info"
- Support more meteorological sensors: Wind speed and direction, rain.

## First steps

- Checkout the repository at a desired location on the Raspberry Pi:

```.sh
git clone https://gitlab.com/poinck/lucht
```

- Install a webserver on the Raspberry Pi (the default configuration should be enough depending on the packaged version by your distributor)
- It is recommended to seperate logging and everything else (forecast and hosting plots) on two different computers. The "lucht-rsync.timer" can be used to transfer measurements from the logger-host to the beeldscherm-host.

## Measurenent on Arduino

### Compile and upload ("flashing")

Stop lucht-capture.service before uploading a new version to Arduino.

```.sh
cd ./arduino
arduino-cli compile --fqbn arduino:avr:leonardo lucht/lucht.ino
arduino-cli upload --log-level debug -v -p /dev/ttyACM0 --fqbn arduino:avr:leonardo lucht
```

### Modified libraries

Following modifcation are depricated since 2022-10-29:

- SHT85: removed constructor for SHT35 (resolve conflict with Seeed SHT35 library)
- Seeed SHT35: refactored NO_ERROR to NO_SHT_ERROR (resolve conflict with other library)

## Logging on Linux

- Connect your Arduino to the Raspberry Pi via USB _(see next chapter for flashing your Arduino first)_
- Configure `~/.luchtrc` (example for running `lucht` on the logging Raspberry Pi):

```.sh
# required options on all hosts
lucht_path="/home/user/gits/lucht/"
    # path where you checked out this repository
data_path="/home/user/data/lucht/"
    # path where logged measurement data will be stored and read
    
# options required on the logger
rsync_target="user@beeldscherm_hostname:data/lucht/"
    
# recomended options to set on the beeldscherm-host
lucht_host="http://lucht/"
    # host that is used to fetch the data
web=1
    # tells `pi/capture_acm.sh` to publish plotted measurements and
    # forecast on the local web server
lucht_webdir="/var/www/html/"
    # web directory of your web server
station_height=39
    # height above mean sea level (this will be used to correct the
    # pressure measurements)
remote=1
    # configuration paremeter "lucht_remote_copy_command" will be executed
    # every 3 beeldscherm (24-hour-plot suitable for e-ink displays) interval
    # (default: 30 minutes).
    # - any desired command can be executed here, eg.:
lucht_remote_copy_command="scp {$lucht_web_dir}beeldscherm/beeldscherm.png user@domain.tld:."

# further optional parameter for other hosts accessing data provided by the
# logger- and beeldscherm-host
desktop=0
    # if set to 1, `lucht` will not delete the previously generated plot
    # at "./lucht.png" (this is benificial for image viewers like "eog")
```

- You can test whether the logging is running correctly by mannually running `./pi/acm_capture.sh` and check with `tail -f _data/lucht.csv`
- Copy `config/systemd/system/lucht-capture.service` to /etc/systemsd/system/ and configure the user name and path to `pi/capture_acm.sh` with your favourite editor.

```.sh
systemctl enable lucht-capture.service
systemctl start lucht-capture.service
systemctl status lucht-capture.service
```

## Plot and display measurements and forecasts

TODO.

- You can test whether the plotting is running correctly by mannually running `./logger/lucht`

## Beeldscherm (24-hour-plot)

This plot is suitable for e-ink displays. Use systemd-service and -timer from `./config/systemd/system/lucht-beeldscherm.{service|timer}` and copy them to "/etc/systemd/system"-folder, edit to your environment needs and enable:

```.sh
systemctl enable lucht-beeldscherm.timer
systemctl start lucht-beeldscherm.timer
systemctl status lucht-beeldscherm.timer
systemctl status lucht-beeldscherm.service
```

### Configure DWD forecasts

Forecasted station data is gathered from "http://opendata.dwd.de". The station_id can be changed in `beeldscherm/lucht-forecast`:

```.sh
#station_id="10385" # SXL
station_id="10384"  # Tempelhof, EDDI
```

## ESP8266 integration

There is a sample configuration for the SHT35 and SCD4X CO2 sensor in `esphome/livingroom_example.yaml` to setup an ESP8266 for use with lucht. See `esphome/*.html` for more information (copied official documentation from esphome.io, 2023-12-21).

In your `~/.luchtrc` you can add these lines to add room temperature, humidity and CO2 levels to your beeldschermn.

```.sh
esp_endpoint="http://esp/sensor/"
esp_co2="co2"
esp_t="t"
esp_hum="hum"
```

## License

Public domain or Un-License for all files except modified libraries.


