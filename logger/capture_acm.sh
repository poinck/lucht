#!/bin/bash
# capture Arduino output (measurements) via ACM tty device
# and save to seperate data directory

. ~/.luchtrc

# configure
ACM_DEV="/dev/ttyACM0"
LUCHT_PATH="${lucht_path}"
DATA_DIR="${data_path}"

# wait
echo "warmup 23 seconds"
sleep 23
echo "start capture"

# capture from device
stty -F ${ACM_DEV} 9600 cread igncr
cat ${ACM_DEV} >> ${DATA_DIR}acm.csv &
acm_pid=$!
echo "acm_pid = ${acm_pid}"

trap ctrl_c INT

ctrl_c() {
    kill ${acm_pid}
    echo "capture stopped"
}

# attach date and convert last measurement to json
tail -n 0 -f ${DATA_DIR}acm.csv | \
while read -r line ; do
    mdate=$( date --utc --iso-8601=min )
    echo "${mdate},${line}" >> ${DATA_DIR}lucht.csv

    # last 5 days
    tail -n 7500 ${DATA_DIR}lucht.csv > ${DATA_DIR}lucht_5d.csv
done

    #lastm=$( tail -n 1 ${DATA_DIR}lucht.csv )
    #lastm1="${lastm//,/ }"
    #status=""
    #status_texts=""
    #i=1
    #jsons="{"
    #for lastv in ${lastm1} ; do
	   # if [ "${i}" -eq 1 ] ; then
	   #     jsons="${jsons}\"datetime\":\"${lastv}\","
	   # elif [ "${i}" -eq 2 ] ; then
	   #     jsons="${jsons}\"pm1\":${lastv},"
	   # elif [ "${i}" -eq 3 ] ; then
	   #     jsons="${jsons}\"pm4\":${lastv},"
	   # elif [ "${i}" -eq 11 ] ; then
	   #     jsons="${jsons}\"ta\":${lastv},"
	   # elif [ "${i}" -eq 12 ] ; then
	   #     jsons="${jsons}\"hum\":${lastv},"
	   # elif [ "${i}" -eq 15 ] ; then
	   #     jsons="${jsons}\"p\":${lastv},"
	   # elif [ "${i}" -eq 14 ] ; then
	   #     jsons="${jsons}\"t_logger\":${lastv},"
	   # elif [ "${i}" -eq 4 ] ; then
    #        status="${status}${lastv}"
    #        #if [ "${lastv}" -eq 1 ] ; then
    #        #    status_texts="${status_texts} sps probing failed "
    #        #elif [ "${lastv}" -eq 2 ] ; then
    #        #    status_texts="${status_texts} sps auto clean interval not set "
    #        #elif [ "${lastv}" -eq 3 ] ; then
    #        #    status_texts="${status_texts} sps start failed "
    #        #elif [ "${lastv}" -eq 4 ] ; then
    #        #    status_texts="${status_texts} sps i2c buffer problem "
    #        #elif [ "${lastv}" -eq 5 ] ; then
    #        #    status_texts="${status_texts} sps ready flag unreadable "
    #        #elif [ "${lastv}" -eq 6 ] ; then
    #        #    status_texts="${status_texts} sps data not ready after 10 retries "
    #        #elif [ "${lastv}" -eq 7 ] ; then
    #        #    status_texts="${status_texts} sps could not read measurement "
    #        #fi
	   # elif [ "${i}" -eq 7 ] ; then
    #        status="${status}${lastv}"
    #        #if [ "${lastv}" -eq 1 ] ; then
    #        #    status_texts="${status_texts} sht1 init failed "
    #        #elif [ "${lastv}" -eq 2 ] ; then
    #        #    status_texts="${status_texts} sht1 could not read measurement "
    #        #fi
	   # elif [ "${i}" -eq 10 ] ; then
    #        status="${status}${lastv}"
    #        #if [ "${lastv}" -eq 1 ] ; then
    #        #    status_texts="${status_texts} sht2 init failed "
    #        #elif [ "${lastv}" -eq 2 ] ; then
    #        #    status_texts="${status_texts} sht2 could not read measurement "
    #        #fi
	   # elif [ "${i}" -eq 13 ] ; then
    #        status="${status}${lastv}"
    #        #if [ "${lastv}" -eq 1 ] ; then
    #        #    status_texts="${status_texts} shtm without sht1 "
    #        #elif [ "${lastv}" -eq 2 ] ; then
    #        #    status_texts="${status_texts} shtm without sht2 "
    #        #elif [ "${lastv}" -eq 3 ] ; then
    #        #    status_texts="${status_texts} shtm without sht1 and 2 "
    #        #fi
	   # elif [ "${i}" -eq 16 ] ; then
    #        status="${status}${lastv}"
    #        #if [ "${lastv}" -eq 1 ] ; then
    #        #    status_texts="${status_texts} icp not connected "
    #        #fi
	   # elif [ "${i}" -eq 19 ] ; then
    #        status="${status}${lastv}"
    #        #if [ "${lastv}" -eq 1 ] ; then
    #        #    status_texts="${status_texts} sht3 init failed "
    #        #elif [ "${lastv}" -eq 2 ] ; then
    #        #    status_texts="${status_texts} sht3 could not read measurement "
    #        #fi
    #    fi
    #    #echo "${lastv}"
	   # i=$(( i+1 ))
    #done
    #if [ "${status}" == "000000" ] ; then
    #    status="ok"
    #else
    #    status="failure_${status}"
    #fi
    #jsons="${jsons}\"status\":\"${status}\"}"
    ##echo "${jsons}"
    #echo "${jsons}" | jq > ${DATA_DIR}lucht.json
        # ! lucht.json is deprecated
