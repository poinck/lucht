#!/bin/bash

. ~/.luchtrc

if [ -n "${rsync_target}" ] ; then
    rsync -q ${data_path}luch* ${rsync_target}
    #rsync -q ${WWW_DIR}lucht_5d.csv ${rsync_target}
    #rsync -q ${WWW_DIR}lucht.json ${rsync_target}
fi