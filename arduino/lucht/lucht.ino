#include <sps30.h>
#include <icp101xx.h>
#include <Wire.h>
#include <SHTSensor.h>

// variables for measurements
float sps_pm1;
float sps_pm25;
float sps_pm4;
float sps_pm10;
int sps_err = 0;
float sht_ta;
float sht_hum;
int sht_err = 0;
float sht_ta2;
float sht_hum2;
int sht_err2 = 0;
float sht_ta_mean;
float sht_hum_mean;
int sht_err_mean = 0;
float icp_ta;
float icp_pa;
int icp_err = 0;
float sht_ta3;
float sht_hum3;
int sht_err3 = 0;

ICP101xx icp;

SHTSensor sensor(SHTSensor::SHT85);
SHTSensor sensor2(SHTSensor::SHT85);

void muxer(uint8_t bus) {
  Wire.beginTransmission(0x70);
  Wire.write(1 << bus);
  Wire.endTransmission();
}

void setup() {
  int16_t ret;
  uint8_t auto_clean_days = 4;
  uint32_t auto_clean;
  int lcd_error;

  Serial.begin(9600);
  delay(2000);

  //Serial.println("DEBUG:setup");

  //muxer(0);

  Wire.begin();

  // SPS30
  sensirion_i2c_init();

  int attempts_sps = 0;
  while (attempts_sps < 10) {
    if (sps30_probe() != 0) {
      sps_err = 1;
        // SPS sensor probing failed
      delay(500);
      attempts_sps = attempts_sps + 1;
    } else {
      sps_err = 0;
      attempts_sps = 11;
    }
  }

  ret = sps30_set_fan_auto_cleaning_interval_days(auto_clean_days);
  if (ret) {
    sps_err = 2;
      // error setting the auto-clean interval
  }

  ret = sps30_start_measurement();
  if (ret < 0) {
    sps_err = 3;
      // error starting measurement
  }

#ifdef SPS30_LIMITED_I2C_BUFFER_SIZE
  sps_err = 4;
      // arduino hardware limitation: mass only
      // https://github.com/Sensirion/arduino-sps#esp8266-partial-legacy-support
  delay(2000);
#endif

  //Serial.println("DEBUG:sps");

  // SHT85s via mux
  muxer(1);
  if (sensor.init()) {
    //Serial.println("sht85 1 ok");
    sht_err = 0;
  } else {
    //Serial.println("DEBUG:sht85 1 NOT ok");
    sht_err = 1;
  }
  delay(1000);

  muxer(2);
  if (sensor2.init()) {
    //Serial.println("sht85 2 ok");
    sht_err2 = 0;
  } else {
    //Serial.println("DEBUG:sht85 2 NOT ok");
    sht_err2 = 1;
  }
  delay(1000);

  //Serial.println("DEBUG:sht85");

  // ICP10125 pressure
  icp.begin();
  delay(1000);

  //Serial.println("DEBUG:icp");

  //lcd.setCursor(0, 1);
  //lcd.print("_initialized");
}

unsigned long time_interval = 60000;
  // measurements every 60 seconds

void loop() {
  struct sps30_measurement m;
  char serial[SPS30_MAX_SERIAL_LEN];
  uint16_t data_ready;
  int16_t ret;
  int ret_count;

  unsigned long stime = millis();

  //Serial.println("DEBUG:loop");

  ret_count = 0;
  do {
    ret = sps30_read_data_ready(&data_ready);
    if (ret < 0) {
      sps_err = 5;
        // error reading data-ready flag
    } else if (!data_ready) {
      ret_count = ret_count + 1;
      sps_err = 6;
        // data not ready, no new measurement available
    } else {
      break;
    }
    delay(100); // retry in 100ms
    //Serial.print("?");
    //Serial.println();
  } while (ret_count < 10);

  //Serial.println("c");

  ret = sps30_read_measurement(&m);
  if (ret < 0) {
    sps_err = 7;
        // error reading measurement
  } else {
    sps_pm1 = m.mc_1p0;
    //sps_pm25 = m.mc_2p5;
    sps_pm4 = m.mc_4p0;
    //sps_pm10 = m.mc_10p0;
    sps_err = 0;
  }
  delay(100);

  // SHT85s via mux
  muxer(1);
  if (sensor.readSample()) {
    sht_hum = sensor.getHumidity();
    sht_ta = sensor.getTemperature();
    sht_err = 0;
//    Serial.print(sht_hum, 1);
//    Serial.print(" ");
//    Serial.println(sht_ta, 1);
  } else {
    //Serial.println("sht85 1 read FAILURE");
    sht_err = 2;
  }
  delay(100);

  muxer(2);
  if (sensor2.readSample()) {
    sht_hum2 = sensor2.getHumidity();
    sht_ta2 = sensor2.getTemperature();
    sht_err2 = 0;
//    Serial.print(sht_hum2, 1);
//    Serial.print(" ");
//    Serial.println(sht_ta2, 1);
  } else {
    //Serial.println("sht85 2 read FAILURE");
    sht_err2 = 2;
  }
  muxer(0);
  delay(100);

  // average of both SHT85 sensors
  if (sht_err == 0) {
    if (sht_err2 == 0) {
      sht_ta_mean = (sht_ta + sht_ta2) / 2;
      sht_hum_mean = (sht_hum + sht_hum2) / 2;
    } else {
      sht_ta_mean = sht_ta;
      sht_hum_mean = sht_hum;
      sht_err_mean = 2;
        // sensor 2 not connected or malfunctioning
    }
  } else {
    if (sht_err2 == 0) {
      sht_ta_mean = sht_ta2;
      sht_hum_mean = sht_hum2;
      sht_err_mean = 1;
        // sensor 1 not connected or malfunctioning
    } else {
      sht_err_mean = 3;
    }
  }

  // measurements ICP10125
  if (!icp.isConnected()) {
    icp_err = 1;
      // sensor not responding
  } else {
    // Start measurement cycle, waiting until it is completed.
    // Optional: Measurement mode
    //    sensor.FAST: ~3ms
    //    sensor.NORMAL: ~7ms (default)
    //    sensor.ACCURATE: ~24ms
    //    sensor.VERY_ACCURATE: ~95ms
    icp.measure(icp.ACCURATE);

    // Read and output measured temperature in Celsius and pressure in Pascal.
    //Serial.print(sensor.getTemperatureC());
    icp_ta = icp.getTemperatureC();
    //Serial.println(reference_pressure - sensor.getPressurePa());
    icp_pa = icp.getPressurePa() / 100;
      // convert to hPa
    icp_err = 0;
  }
  delay(100);

  Serial.print(sps_pm1);
  Serial.print(",");
  Serial.print(sps_pm4);
  Serial.print(",");
  Serial.print(sps_err);
  Serial.print(",");
  Serial.print(sht_ta);
  Serial.print(",");
  Serial.print(sht_hum);
  Serial.print(",");
  Serial.print(sht_err);
  Serial.print(",");
  Serial.print(sht_ta2);
  Serial.print(",");
  Serial.print(sht_hum2);
  Serial.print(",");
  Serial.print(sht_err2);
  Serial.print(",");
  Serial.print(sht_ta_mean);
  Serial.print(",");
  Serial.print(sht_hum_mean);
  Serial.print(",");
  Serial.print(sht_err_mean);
  Serial.print(",");
  Serial.print(icp_ta);
  Serial.print(",");
  Serial.print(icp_pa);
  Serial.print(",");
  Serial.print(icp_err);
  Serial.print(",");
  Serial.print(sht_ta3);
  Serial.print(",");
  Serial.print(sht_hum3);
  Serial.print(",");
  Serial.print(sht_err3);
  Serial.println();
    // CSV signature:
    //pm1,pm4,sps_err,ta1,hum1,sht_err1,ta2,hum2,sht_err2,ta_mean,hum_mean,mean_err,
    // ta3,hum3,sht_err3

  // check measurement time to reduce drift
  unsigned long etime = millis();
  unsigned long dtime = etime - stime;
  if (dtime > time_interval) {
    dtime = time_interval - 10;
  }
  //Serial.println(dtime);
  delay(time_interval - dtime);
    // subtract drift in ms (should be independed of receiving
    // device, because of fixed baud rate of 9600)?
}
